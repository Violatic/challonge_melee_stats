import challonge
import pandas as pd
from utils import participant_df_creator
import keyring

tournament_series = "4qs"
# Tell pychallonge about your [CHALLONGE! API credentials](http://api.challonge.com/v1).
challonge.set_credentials("Violatic", keyring.get_password("challonge", "Violatic"))

full_tournament_df = pd.DataFrame(columns=['display_name'])
for event_no in range(0, 200):
    true_event_no = event_no + 1

    # Retrieve a tournament by its id (or its url).
    try:
        full_tournament_df = participant_df_creator("4qs-4qm",
                                                    full_tournament_df,
                                                    tournament_series,
                                                    true_event_no
                                                    )
    except:
        try:
            full_tournament_df = participant_df_creator("4qsuground",
                                                        full_tournament_df,
                                                        tournament_series,
                                                        true_event_no)
        except:
            try:
                full_tournament_df = participant_df_creator("4qm",
                                                            full_tournament_df,
                                                            tournament_series,
                                                            true_event_no
                                                            )
            except:
                pass

full_tournament_df.to_csv("results/" + tournament_series + ".csv", index=False)
