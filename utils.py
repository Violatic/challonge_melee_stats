import challonge
import pandas as pd
from fuzzywuzzy import process
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def next_power_of_2(x):
    return 1 if x == 0 else 2**(x - 1).bit_length()


def participant_df_creator(url, sub_tournament_df, event_name, event):
    tournament_id = url + str(event)
    tournament = challonge.tournaments.show(tournament_id)
    participants = challonge.participants.index(tournament["id"])
    result_str = event_name + " - result - " + str(event)
    performance_str = event_name + " - performance - " + str(event)

    player_df = pd.DataFrame(participants, columns=['display_name', 'final_rank'])
    player_df = player_df.rename(columns={"final_rank": result_str})

    player_df[performance_str] = player_df[result_str] / next_power_of_2(player_df.shape[0])
    remap_df = pd.read_csv("remappings/" + event_name + ".csv")
    remap_dict = pd.Series(remap_df.remapping.values, index=remap_df.entered_tag).to_dict()
    player_df = player_df.replace({"display_name": remap_dict})

    choices = list(sub_tournament_df['display_name'])
    player_names = list(player_df['display_name'])

    for player_name in player_names:
        closest_matches = process.extract(player_name, choices, limit=1)
        if not closest_matches:
            continue

        closest_match_name = closest_matches[0][0]
        closest_match_score = closest_matches[0][1]
        if closest_match_score == 100:
            continue

        if closest_match_score > 90:
            print("closest_match_name: " + str(closest_match_name) + ". score: " + str(closest_match_score))
            print("Reassigning " + str(player_name) + " to a new tag = " + str(closest_match_name))
            player_df.loc[player_df['display_name'] == player_name, 'display_name'] = str(closest_match_name)
        else:
            print("New player : " + str(player_name))
    try:
        sub_tournament_df = pd.merge(player_df, sub_tournament_df, on='display_name', how='outer')
        sub_tournament_df = sub_tournament_df.drop_duplicates(subset=['display_name'])
    except:
        sub_tournament_df = sub_tournament_df if not sub_tournament_df.empty else player_df
    return sub_tournament_df


def all_time_plot(input_df, tournament_name, graph_measure):
    plot_measure = "mean_" + graph_measure
    input_df = input_df.sort_values(by=plot_measure)
    bar_line_graph(plot_measure, tournament_name, input_df)
    return


def bar_line_graph(measure_string, bl_tournament_label, plot_df):
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    # Add traces
    fig.add_trace(go.Bar(x=plot_df['display_name'],
                         y=plot_df['attendance'],
                         name="attendance",
                         opacity=0.5),
                  secondary_y=True,
                  )

    # Add traces
    fig.add_trace(go.Scatter(x=plot_df['display_name'],
                             y=plot_df[measure_string],
                             name=measure_string,
                             mode='lines+markers'),
                  secondary_y=False,
                  )

    # Add figure title
    fig.update_layout(
        title_text=bl_tournament_label + " Results by Performance"
    )

    # Set x-axis title
    fig.update_xaxes(title_text="Player")

    # Set y-axes titles
    fig.update_yaxes(title_text=measure_string, secondary_y=False)
    fig.update_yaxes(title_text="Attendance", secondary_y=True)

    fig.show()
    return


def metrics_calc(tournament_results_df, calc_result=0, calc_performance=0):
    if calc_result == 1:
        results_cols = [s for s in list(tournament_results_df) if "result" in s]
        tournament_results_df.loc[:, 'mean_result'] = tournament_results_df[results_cols].mean(axis=1).astype(float)
        tournament_results_df.loc[:, 'attendance'] = tournament_results_df[results_cols].count(axis=1)
    if calc_performance == 1:
        performance_cols = [s for s in list(tournament_results_df) if "performance" in s]
        tournament_results_df.loc[:, 'mean_performance'] = tournament_results_df[
            performance_cols].mean(axis=1).astype(float)
        if calc_result == 0:
            tournament_results_df.loc[:, 'attendance'] = tournament_results_df[performance_cols].count(axis=1)

    return tournament_results_df


def event_exclude(ar_input_df, min_attendance, event_str, events_to_include=10000):
    results_cols = [s for s in list(ar_input_df) if "result" in s]
    split_list_of_lists = [x.split(" - ") for x in results_cols]
    event_nums_list = [x[-1] for x in split_list_of_lists]
    # Since they are a list of strings, we need to map to a list of integers
    event_nums_list = list(map(int, event_nums_list))
    event_nums_list.sort()
    most_recent_n_event_nums = event_nums_list[-events_to_include:]

    most_recent_n_results = [event_str + " - result - " + str(x) for x in most_recent_n_event_nums]
    most_recent_n_performances = [event_str + " - performance - " + str(x) for x in most_recent_n_event_nums]
    most_recent_n_events = most_recent_n_performances + most_recent_n_results

    n_recent_df = ar_input_df[['display_name'] + most_recent_n_events]
    n_recent_df = metrics_calc(n_recent_df, calc_performance=1, calc_result=1)
    included_events_df = n_recent_df.loc[n_recent_df['attendance'] >= min_attendance]
    return included_events_df
