import challonge
import pandas as pd
import keyring

# Tell pychallonge about your [CHALLONGE! API credentials](http://api.challonge.com/v1).
challonge.set_credentials("Violatic", keyring.get_password("challonge", "Violatic"))
tournament_series = "LLN"

full_tournament_df = pd.DataFrame()
for event_no in range(0, 200):
    true_event_no = event_no + 1
    tournament_id = tournament_series + str(true_event_no)

    # Retrieve a tournament by its id (or its url).
    try:
        tournament = challonge.tournaments.show(tournament_id)
        participants = challonge.participants.index(tournament["id"])
        player_df = pd.DataFrame(participants, columns=['display_name', 'final_rank'])
        player_df = player_df.rename(columns={"final_rank": tournament_series + " - " + str(true_event_no)})
        try:
            full_tournament_df = pd.merge(player_df, full_tournament_df, on='display_name', how='outer')
        except KeyError:
            full_tournament_df = full_tournament_df if not full_tournament_df.empty else player_df
    except:
        pass

full_tournament_df.to_csv("results/" + tournament_series + ".csv")
