import pandas as pd
import plotly.io as pio
from utils import all_time_plot, event_exclude
import plotly.express as px
import math

pio.renderers.default = "browser"
event_name = "4qs"
number_of_events = 2
min_attendance_req = 15
full_tournament_df = pd.read_csv("results/" + event_name + ".csv")

all_df = event_exclude(full_tournament_df,
                       min_attendance=min_attendance_req,
                       event_str=event_name)
all_time_plot(all_df, event_name, "performance")
all_time_plot(all_df, event_name, "result")

n_recent_df = event_exclude(full_tournament_df,
                            min_attendance=5,
                            events_to_include=25,
                            event_str=event_name)

all_time_plot(n_recent_df, event_name, "performance")
all_time_plot(n_recent_df, event_name, "result")


# TODO: Everything below this needs cleaning up and moving to utils
performance_cols = [s for s in list(full_tournament_df) if "performance" in s]
results_cols = [s for s in list(full_tournament_df) if "result" in s]
melt_cols = ['display_name'] + performance_cols
melt_df = full_tournament_df[melt_cols].melt('display_name', var_name='cols', value_name='vals')
melt_df = melt_df.loc[(melt_df['display_name'] == 'Vio') | (melt_df['display_name'] == 'Skips')]

fig = px.line(x="cols",
              y="vals",
              color="display_name",
              data_frame=melt_df,
              markers=True)
fig.show()


def n_round(x, base=5):
    return base * round(x / base)


rolling_n = 10
res_perf = "performance"
split_list_of_lists = [x.split(" - ") for x in results_cols]
event_nums_list = [x[-1] for x in split_list_of_lists]
# Since they are a list of strings, we need to map to a list of integers
event_nums_list = list(map(int, event_nums_list))
most_recent_event = max(event_nums_list)
# TODO: this currently doesn't account for the fact that we want to include next highest n so if 183 then we want the
#  highest boundary to be 190 for a rolling_n of 10
no_rolling_cats = n_round(most_recent_event, rolling_n) / rolling_n
no_rolling_cats = int(no_rolling_cats)
for cat_no in range(0, no_rolling_cats):
    rolling_n_possible_events = [(cat_no * rolling_n) + x for x in range(1, rolling_n + 1)]
    rolling_n_existing_events = list(set(rolling_n_possible_events).intersection(event_nums_list))
    included_rolling_col_names = [str(event_name) + " - " + str(res_perf) + " - " + str(x)
                                  for x in rolling_n_existing_events]
    largest_n = max(rolling_n_existing_events)
    smallest_n = min(rolling_n_existing_events)
    # TODO: this does not flag any missing tournaments from the dataset that lie between largest and smallest
    n_rolled_col = str(event_name) + " - rolling " + str(res_perf) + " - " + str(smallest_n) + " to " + str(largest_n)
    full_tournament_df[n_rolled_col] = full_tournament_df[included_rolling_col_names].mean(axis=1)

rolling_results_cols = [s for s in list(full_tournament_df) if "rolling " + str(res_perf) in s]
melt_cols = ['display_name'] + rolling_results_cols
melt_value = "average " + res_perf
melt_df = full_tournament_df[melt_cols].melt('display_name', var_name='event', value_name=melt_value)
melt_df = melt_df.loc[(melt_df['display_name'] == 'Vio') |
                      (melt_df['display_name'] == 'Skips') |
                      (melt_df['display_name'] == 'AC') |
                      # (melt_df['display_name'] == 'Silas') |
                      # (melt_df['display_name'] == 'Moby') |
                      (melt_df['display_name'] == 'Midori')]

fig = px.line(x="event",
              y=melt_value,
              color="display_name",
              data_frame=melt_df,
              markers=True
              )
fig.show()
